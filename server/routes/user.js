var express = require('express');
var router = express.Router();
const checkAuth = require('../middleware/auth')

const UserController =require( '../controllers/user')

router.post('/login', UserController.login)
router.post('/signup', UserController.signup);
router.delete('/:id', UserController.delete)
router.get('/:id', checkAuth, UserController.get_user_by_id)
router.get('/view-profile/:id', checkAuth, UserController.get_user_for_view)
router.patch('/:id', checkAuth, UserController.update_user_by_id)
router.post("/forgot-password", UserController.forgot_password)
router.post("/reset-password", UserController.reset_password)

module.exports = router;
const path = require('path');
const http = require('http');
const express = require('express');
const cookieParser = require('cookie-parser');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const app = express();
const mongoose = require('mongoose')

var cors = require ('cors');

app.use(cors({
    origin:['http://localhost:4200','http://127.0.0.1:4200'],
    credentials:true
}));

var productsRouter = require('./routes/product');
var orderRouter = require('./routes/order');
var usersRouter = require('./routes/user');
var menuRouter = require('./routes/menu');

mongoose.connect(
    'mongodb+srv://khmtgroup02:' +
     process.env.MONGO_ATLAS_PW + 
     '@cluster0-zwf9v.mongodb.net/khmtgroup02?retryWrites=true&w=majority',
     { 
        useNewUrlParser: true, 
        useUnifiedTopology: true,
        useCreateIndex: true,
    }
)

mongoose.Promise = global.Promise

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json()); 

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
        'Access-Control-Allow-Headers', 
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
        );
    res.header('Access-Control-Allow-Credentials', true);
    if(req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }
    next();
})

app.use('/product', productsRouter);
app.use('/user', usersRouter);
app.use('/order', orderRouter);
app.use('/menu', menuRouter);

app.use((req, res, next) => {
    const error = new Error("Not found");
    error.status = 404;
    next(error);
})

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
         error : {
            message: error.message
        }})
})
module.exports = app;
import { HttpClientModule } from '@angular/common/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import {Observable} from 'rxjs';
import { resolve } from 'url';
const httpOptions = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Origin': '*',
    'Authorization': 'authkey',
    'userid': '1',
    'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS'
  })
};
// import { FirebaseService } from './firebase.service';
@Injectable()
export class AuthService {
  private auth: any;
  constructor( private http: HttpClient) {

  }
  // Login(value){
  //   // console.log(value.email)
  //   //
  // }
  Login(value){
    return new Promise(( resolve, reject) =>{
       this.http.post('http://localhost:4000/user/login', value, httpOptions).toPromise().then((res) =>{
        console.log(res);
        console.log('Done');
      });
    });
  }
  GetMenu(){
    return new Promise(( resolve, reject) =>{
      this.http.get('http://localhost:4000/menu', httpOptions).toPromise().then((res) =>{
        resolve(res);
     });
   });
  }
}

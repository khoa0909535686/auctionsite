import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserDetailRoutingModule } from './user-detail-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    UserDetailRoutingModule,
    ReactiveFormsModule
  ],
  declarations: []
})
export class UserDetailModule { }

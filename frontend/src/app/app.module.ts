import { AuthService } from './../services/auth.services';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { LoginModule } from './login/login.module';
import { SignupComponent } from './signup/signup.component';
import { SignupModule } from './signup/signup.module';
import { HomeComponent } from './home/home.component';
import { HomeModule } from './home/home.module';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserDetailModule } from './user-detail/user-detail.module';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { Platform } from '@angular/cdk/platform';
import {ObserversModule, ContentObserver} from '@angular/cdk/observers';
import { AutofillMonitor } from '@angular/cdk/text-field';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {A11yModule, HighContrastModeDetector, FocusMonitor} from '@angular/cdk/a11y';
const appRouts: Routes = [
  { path: '**', component: UserDetailComponent},
  { path: 'login', component: LoginComponent},
  {path: 'user-detail', component: UserDetailComponent},
 { path : 'home' , component: HomeComponent}];
@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    HomeComponent,
    UserDetailComponent,
  ],
  imports: [
    BrowserModule,
    MatFormFieldModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatInputModule,
    ObserversModule,
    RouterModule.forRoot(
      appRouts,
      {enableTracing : true}
    ),
    LoginModule,
    SignupModule,
    HomeModule,
    UserDetailModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    Platform,
    AutofillMonitor,
    ContentObserver,
    HighContrastModeDetector,
    FocusMonitor,
    AuthService,
  ],
  bootstrap: [AppComponent],
  // exports:[
  //   HighContrastModeDetector
  // ]
})
export class AppModule { }


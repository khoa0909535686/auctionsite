import { AuthService } from './../../services/auth.services';

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private auth: AuthService,
    private router: Router
    ) {}
  errorMessage: String = '';
  LoginForm: FormGroup;
  returnURL: string;
  loading = false;
  validation_messages = {
    email: [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Please enter a valid email.' }
    ],
    password: [
      { type: 'required', message: 'Password is required.' },
      {
        type: 'minlength',
        message: 'Password must be at least 5 characters long.'
      }
    ]
  };
  submitted = false;
  ngOnInit() {
    this.LoginForm = this.formBuilder.group({
      email: new FormControl(
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
        ])
      ),
      password: new FormControl(
        '',
        Validators.compose([Validators.minLength(5), Validators.required])
      )
    });
    this.returnURL = this.route.snapshot.queryParams['returnUrl'] || '/';
  }
  onSubmit(value) {
    this.submitted = true;
     this.auth.Login(value).then((res) => {
      this.router.navigate(['/user-detail']);
       console.log(res);
     });
    this.loading = true;
  }
}

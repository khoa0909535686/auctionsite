import { AuthService } from './../../services/auth.services';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import * as $ from 'jquery';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router) { }

  errorMessage: String = '';
  signupForm: FormGroup;
  returnURL: string;
  loading = false;
  ChoosenCity: any;
  validation_messages = {
    firstName: [
      { type: 'required', message: 'First name is required.' },
      { type: 'pattern', message: 'Please enter a valid first name.' }
    ],
    lastName: [
      { type: 'required', message: 'Last name is required.' },
      { type: 'pattern', message: 'Please enter a valid last name.' }
    ],
    email: [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Please enter a valid email.' }
    ],
    password: [
      { type: 'required', message: 'Password is required.' },
      {
        type: 'minlength',
        message: 'Password must be at least 5 characters long.'
      }
    ],
    phoneNumber: [
      { type: 'required', message: 'Phone number is required.' },
      { type: 'pattern', message: 'Please enter a valid phone number.' },
      { type: 'minlength', message: 'Please enter a valid phone number.' }
    ],
  };
  submitted = false;


  ngOnInit() {
      this.signupForm = this.formBuilder.group({
        firstName: new FormControl(
          '',
          Validators.compose([
            Validators.required,
            Validators.pattern('^[a-z]+$')
          ])
        ),
        lastName: new FormControl(
          '',
          Validators.compose([
            Validators.required,
            Validators.pattern('^[a-z]+$')
          ])
        ),
        email: new FormControl(
          '',
          Validators.compose([
            Validators.required,
            Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
          ])
        ),
        password: new FormControl(
          '',
          Validators.compose([Validators.minLength(5), Validators.required])
        ),
        phoneNumber: new FormControl(
          '',
          Validators.compose([
            Validators.required,
            Validators.pattern('^[0-9]+$'),
            Validators.minLength(10)
          ])
        )
      });
      this.returnURL = this.route.snapshot.queryParams['returnUrl'] || '/';
      let Districtdropdown = $('#District_dropDown');
      Districtdropdown.length = 0 ;
      Districtdropdown.empty();
      Districtdropdown.append('<option selected="true" disabled>Choose State/Province</option>');
      Districtdropdown.prop('selectedIndex', 0);
      $.getJSON('assets/vn.json', function(json) {
        $.each(json, function (key, entry) {
          $('#District_dropDown').append(`<option value="${entry.name}">
          ${entry.name}
     </option>`);
        });
      });
      $('#District_dropDown').change(function() {
        var selectedVal = $('#District_dropDown option:selected').val();
        console.log(selectedVal);
        let CityDistrictdropdown = $('#CityDistrict_dropDown');
        CityDistrictdropdown.length = 0 ;
        CityDistrictdropdown.empty();
        CityDistrictdropdown.append('<option selected="true" disabled>Choose State/Province</option>');
        CityDistrictdropdown.prop('selectedIndex', 0);
        $.getJSON('assets/vn.json', function(json) {
          $.each(json, function (key, entry) {
            console.log(entry[2].districts);
            if(selectedVal === entry.name){
              console.log(entry.districts.values);
            $('#CityDistrict_dropDown').append(`<option value="${entry.key}">
            ${entry.districts}
       </option>`);
            }
          });
        });
   });

  }

  // convenience getter for easy access to form fields
  get f() { return this.signupForm.controls; }

  onSubmit(value) {
      this.submitted = true;
      // stop here if form is invalid
      this.loading = true;
  }

}

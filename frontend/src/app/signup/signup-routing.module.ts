import { SignupComponent } from './signup.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const SingupRoutes: Routes = [
  {path: 'signup', component: SignupComponent}
];
@NgModule({
  imports: [RouterModule.forChild(SingupRoutes)],
  exports: [RouterModule]
})
export class SignupRoutingModule { }

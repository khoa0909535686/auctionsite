import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { AuthService } from './../../services/auth.services';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  Menu: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthService)
    { }

  ngOnInit() {
   this.auth.GetMenu().then((res) => {
      this.Menu = res;
      console.log(this.Menu);
   });
  }

}
